function meatBall(meat, flour, water, recipe) {
    const arrRecipe = [recipe.meat, recipe.flour, recipe.water];

    const arr = ["meat", "flour", "water"];
    const arrSupply = [meat, flour, water];
  
    //Hitung jumlah meatballs yang dapat dihasilkan
    const countMeatBalls = [];
    for (let i = 0; i < arrRecipe.length; i++) {
      countMeatBalls.push(arrSupply[i] / arrRecipe[i]);
    }
  
    const resultMeatBalls = Math.floor(Math.min(...countMeatBalls));
  
    // Hitung sisa bahan
    let remain = [];
    for (let i = 0; i < arrRecipe.length; i++) {
      const remainRecipe = arrSupply[i] - arrRecipe[i] * resultMeatBalls;
      remain.push([arr[i], remainRecipe]);
    }

    const remains = remain.filter(([key, value]) => value !== 0);
  
    for (let item in remains) {
      switch (remains.length) {
        case 3:
          textRemain = `with ${remains[0][1]} ${remains[0][0]}, ${remains[1][1]} ${remains[1][0]}, and ${remains[2][1]} ${remains[2][0]} remain`;
          break;
        case 2:
          textRemain = `with ${remains[0][1]} ${remains[0][0]} and ${remains[1][1]} ${remains[1][0]} remain`;
          break;
        case 1:
          textRemain = `with ${remains[0][1]} ${remains[0][0]} remain`;
          break;
        case 0:
          textRemain = "";
          break;
      }
    }
    return result = `${resultMeatBalls} meat ball ${textRemain}`;
}
  
  function test(fun, result) {
    console.log(fun === result);
  }

  const standardRecipe = {
    meat: 1,
    flour: 2,
    water: 1,
  };
  
  const tastyRecipe = {
    meat: 1.5,
    flour: 1.5,
    water: 1,
  };
  
test(meatBall(30,61.5,30,standardRecipe), '30 meat ball with 1.5 flour remain');
test(meatBall(12,25,11,standardRecipe), '11 meat ball with 1 meat and 3 flour remain');
test(meatBall(23,20,20,tastyRecipe), '13 meat ball with 3.5 meat, 0.5 flour, and 7 water remain');
test(meatBall(42,45,51,standardRecipe), '22 meat ball with 20 meat, 1 flour, and 29 water remain');
test(meatBall(15,55,21,standardRecipe), '15 meat ball with 25 flour and 6 water remain');
test(meatBall(33,24,19,tastyRecipe), '16 meat ball with 9 meat and 3 water remain');
